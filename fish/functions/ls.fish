function ls --wraps='exa --icons -l -h' --description 'alias ls=exa --icons -l -h'
  exa --icons -l -h $argv
        
end
