function la --wraps=ls --wraps='exa --icons -l -h -a' --description 'alias la=exa --icons -l -h -a'
  exa --icons -l -h -a $argv
        
end
