function install --wraps='sudo pacman -Sy' --description 'alias install=sudo pacman -Sy'
  sudo pacman -Sy $argv
end
