function wallpaper --wraps=wallpaper --wraps='feh --randomize --recursive --bg-fill ~/Pictures/Wallpapers/'
  feh --randomize --recursive --bg-fill ~/Pictures/Wallpapers/
end
