function clipin --wraps='xclip -i -selection clipboard' --description 'alias clipin=xclip -i -selection clipboard'
  xclip -i -selection clipboard $argv
        
end
