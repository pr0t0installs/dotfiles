function clipout --wraps='xclip -o -selection clipboard' --description 'alias clipout=xclip -o -selection clipboard'
  xclip -o -selection clipboard $argv
        
end
