function update --wraps='sudo pacman -Syu' --description 'alias update=sudo pacman -Syu'
  if count $argv > 0
        # Update the specified package
        sudo pacman -Sy $argv[1]
    else
        # Update all packages
        sudo pacman -Syu
    end
end
