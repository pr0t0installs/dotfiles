function n --wraps='nvim .' --description 'alias n=nvim .'
  command echo $argv
  if test -n "$argv"
    nvim $argv
  else
    nvim -c "Neotree" -c "wincmd l" -c "quit"
  end
end
