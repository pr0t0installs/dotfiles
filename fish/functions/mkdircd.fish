function mkdircd --wraps=mkdir --description 'alias mkdircd=mkdir'
  command mkdir $argv
  if test $status = 0
    cd $argv
  end
end
