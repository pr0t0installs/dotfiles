if status is-interactive
    # Commands to run in interactive sessions can go here
end

fish_ssh_agent

starship init fish | source

export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
