#!/bin/sh

run() {
    if ! pgrep -f "$1"; then
        "$@" &
    fi
}

run "volumeicon"
run "nm-applet"
run "flameshot"
run "feh" --randomize --recursive --bg-fill ~/Pictures/Wallpapers/ 
# run "cbatticon" -n
# run "" &
